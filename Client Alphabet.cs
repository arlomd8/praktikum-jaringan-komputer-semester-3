﻿
//client

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
namespace client
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient("127.0.0.1", 1200);
            Console.WriteLine("[try to connect the server]");
            NetworkStream n = client.GetStream();
            Console.WriteLine("[connected]");
            string ch;

            Console.Write("Input : ");

            while ((ch = Console.ReadLine()) != "0")
            {
                // mengirim
                byte[] message = Encoding.Unicode.GetBytes(ch);
                n.Write(message, 0, message.Length);


                //menerima
                byte[] bytesToRead = new byte[client.ReceiveBufferSize];
                int data = n.Read(bytesToRead, 0, client.ReceiveBufferSize);
                
                Console.Write("Converted : ");
                Console.Write(Encoding.Unicode.GetString(bytesToRead, 0, data));
              
                Console.WriteLine("");

                Console.Write("Input : ");
            }

            client.Close();
            Console.ReadKey();

        }
    }
}

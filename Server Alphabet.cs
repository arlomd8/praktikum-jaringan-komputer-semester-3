﻿//server
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
namespace serveur_tcp
{
    class Program
    {
        static void Main(string[] args)
        {
            int k = 30;
            //TcpListener listen = new TcpListener(IPAddress.Any, 1200);
            IPAddress ipAd = IPAddress.Parse("127.0.0.1");
            // use local m/c IP address, and 
            // use the same in the client

            /* Initializes the Listener */
            TcpListener listen = new TcpListener(ipAd, 1200);

            Console.WriteLine("[Listening...]");
            listen.Start();
            TcpClient client = listen.AcceptTcpClient();    //incoming client connek
            Console.WriteLine("[Client connected]");
            while (true)
            {
                NetworkStream stream = client.GetStream();  //get incoming data
                byte[] buffer = new byte[client.ReceiveBufferSize];
                int data = stream.Read(buffer, 0, client.ReceiveBufferSize); //read incoming stream
                string ch = Encoding.Unicode.GetString(buffer, 0, data);    //convert data to string
                Console.WriteLine("Message received : " + ch);
                ch = convertString(ch, k);

                // byte[] buffer = encoder.GetBytes("Hello Client!");
                //sending back text
                Console.WriteLine("Converting...");
                Console.WriteLine("Sending Back..." + ch);

                byte[] message = Encoding.Unicode.GetBytes(ch);
                stream.Write(message, 0, message.Length);
                
                
            }

            client.Close();
            Console.ReadKey();


        }

        static string convertString(string s, int k)
        {
                // changed string 
                String newS = "";

                // iterate for every characters 
                for (int i = 0; i < s.Length; ++i)
                {
                    // ASCII value 
                    int val = s[i];
                    // store the duplicate 
                    int dup = k;
                   

                // if k-th ahead character exceed 'z' 

                if (val + k > 122)
                {
                        k -= (122 - val);
                        k = k % 26;
                    if (k == 0)
                        k += 26;
                        newS += (char)(96 + k);
                }
                else if(val == 32)
                {
                    newS += (char)(val); 
                }
                else
                {
                        newS += (char)(96 + k);
                }

                    k = dup;
                }

                // print the new string 
                return(newS);
            
            
        }
    }
}

